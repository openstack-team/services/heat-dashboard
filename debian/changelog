heat-dashboard (12.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090417).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 16:57:34 +0100

heat-dashboard (12.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 16:58:37 +0200

heat-dashboard (12.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 17:15:21 +0200

heat-dashboard (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * (build-)depends on horizonn >= 3:25.0.0.

 -- Thomas Goirand <zigo@debian.org>  Fri, 13 Sep 2024 12:37:33 +0200

heat-dashboard (11.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Apr 2024 15:56:47 +0200

heat-dashboard (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fix clean.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Mar 2024 15:04:07 +0100

heat-dashboard (10.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 11:49:04 +0200

heat-dashboard (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Sep 2023 15:50:51 +0200

heat-dashboard (9.0.0-4) unstable; urgency=medium

  * Removed build-depends on python3-openstack.nose-plugin.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Aug 2023 11:27:52 +0200

heat-dashboard (9.0.0-3) unstable; urgency=medium

  * Cleans properly (Closes: #1044239).

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Aug 2023 15:03:47 +0200

heat-dashboard (9.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 16:21:48 +0200

heat-dashboard (9.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 15:22:25 +0100

heat-dashboard (9.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bookworm.
  * Switch to debhelper 11.
  * Up standards-version to 4.6.1.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Mar 2023 09:24:03 +0100

heat-dashboard (8.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 09:28:49 +0200

heat-dashboard (8.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 18:21:41 +0200

heat-dashboard (8.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed patches applied upstream:
    - Fixed_Unit_test_for_Heat-dashboard.patch
    - django-4-django.conf.urls.url-is-removed.patch
    - django-4-django.utils.urlunquote-is-removed.patch
    - django-4-ugettext_lazy-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Sep 2022 17:10:49 +0200

heat-dashboard (7.0.0-2) unstable; urgency=medium

  * Add Django 4 support patches (Closes: #1015050):
    - django-4-ugettext_lazy-is-removed.patch
    - django-4-django.conf.urls.url-is-removed.patch
    - django-4-django.utils.urlunquote-is-removed.patch
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Thu, 28 Jul 2022 22:57:38 +0200

heat-dashboard (7.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Add Fixed_Unit_test_for_Heat-dashboard.patch.
  * Fixed (build-)depends for the above patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 21:15:15 +0200

heat-dashboard (7.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Mar 2022 13:40:28 +0200

heat-dashboard (7.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed django-3.2-fix-getting-location-header.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2022 11:34:35 +0100

heat-dashboard (6.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:26:25 +0200

heat-dashboard (6.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:48:23 +0200

heat-dashboard (6.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * (Build-)depends on minimum horizon >= 20.0.0+git2020.09.21.27036cc0eb.
  * Add django-3.2-fix-getting-location-header.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Sep 2021 14:18:34 +0200

heat-dashboard (5.0.0-4) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:13:17 +0200

heat-dashboard (5.0.0-3) experimental; urgency=medium

  * Add rm_conffile to remove old files in /etc/openstack-dashboard/enable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 May 2021 11:39:40 +0200

heat-dashboard (5.0.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 May 2021 16:26:45 +0200

heat-dashboard (5.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Apr 2021 22:49:19 +0200

heat-dashboard (5.0.0~rc1-1) experimental; urgency=medium

  * New usptream release.
  * Do not copy heat_policy.json.

 -- Thomas Goirand <zigo@debian.org>  Sat, 27 Mar 2021 21:37:04 +0100

heat-dashboard (4.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 15 Mar 2021 22:09:12 +0100

heat-dashboard (4.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 17:48:26 +0200

heat-dashboard (4.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed fix-unit-tests.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Sep 2020 11:41:42 +0200

heat-dashboard (3.0.0-1) unstable; urgency=medium

  * Fixed Homepage URL.
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 16:44:30 +0200

heat-dashboard (3.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 12:13:19 +0200

heat-dashboard (3.0.0~rc1-1) experimental; urgency=medium

  [ Michal Arbet ]
  * d/patches:
    - Add fix-unit-tests.patch
    - Remove remove-unreliable-test.patch
  * d/control:
    - Bump openstack-dashboard to 18.3.1
    - Remove python3-mock build dependency
  * d/rules: Fix execution of tests as tox do
  * d/copyright: Fix year for debian/*

  [ Thomas Goirand ]
  * Move the package to the horizon-plugins Salsa group.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Deactivate remove-unreliable-test.patch.

 -- Michal Arbet <michal.arbet@ultimum.io>  Sat, 25 Apr 2020 13:19:34 +0200

heat-dashboard (2.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 21:05:30 +0200

heat-dashboard (2.0.0~rc1-1) unstable; urgency=medium

  * New upstream release.
  * Add remove-unreliable-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Oct 2019 21:52:45 +0200

heat-dashboard (1.5.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Michal Arbet ]
  * Do not move files from source, copy it,
    kolla deployment expects that files are in /usr/lib...

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 15 Aug 2019 17:17:44 +0200

heat-dashboard (1.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed versions of packages when satisfied in Buster.
  * Fixed (build-)depends for this release.
  * Do not run integration tests.

 -- Thomas Goirand <zigo@debian.org>  Sun, 31 Mar 2019 21:21:39 +0200

heat-dashboard (1.4.0-2) unstable; urgency=medium

  * Redesign heat-dashboard:
      - Enabled files now in /etc/openstack-dashboard/
      - Removed post scripts which is now achieved by a trigger
      - Update copyright
      - Update Uploaders filed in d/control
  * Add install-missing-files.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 18 Jan 2019 18:44:00 +0100

heat-dashboard (1.4.0-1) unstable; urgency=medium

  [ David Rabel ]
  * d/watch: Add dversionmangle for dfsg tag.

  [ Thomas Goirand ]
  * Fixed postrm syntax.
  * New upstream release:
    - No need to produce a +dfsg version anymore, as the non-dfsg files are
      gone away. Thanks to upstream for that!
    - Removed remove-minified-js.patch.
    - Removed Fix_file_path_typo_for_hotgen-main.scss.patch applied upstream.
  * Fixed (bulid-)depends for this release.
  * Bumped Standards-Version to 4.2.0 (no change needed).

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 May 2018 14:18:27 +0200

heat-dashboard (1.0.2+dfsg1-2) unstable; urgency=medium

  * Add Fix_file_path_typo_for_hotgen-main.scss.patch.

 -- Thomas Goirand <zigo@debian.org>  Sun, 04 Mar 2018 14:16:28 +0100

heat-dashboard (1.0.2+dfsg1-1) unstable; urgency=medium

  * Initial release (Closes: #891002).

 -- Thomas Goirand <zigo@debian.org>  Wed, 21 Feb 2018 14:09:04 +0100
